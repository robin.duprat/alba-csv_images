#!/usr/bin/env python3

import sys
import os
import csv
import glob
import urllib.parse

################################################################################
# Internal functions

def try_open(filename):
    try:
        file = open(filename, "w+")
    except FileNotFoundError:
        os.makedirs(os.path.dirname(filename))
        file = open(filename, "w+")
    return file

def copy_and_rename(file, src_dir, dst_dir, is_dry_run):
    filename = os.path.basename(file)
    split_filename = filename.split("_")
    if len(split_filename) != 2:
        print("Error: image name (%s) shall have only 1 '_' (underscore) - Skip" % filename)
        return (-1, "")
    else:
        try:
            if not is_dry_run:
                os.makedirs(dst_dir + src_dir)
        except FileExistsError:
            pass
        if not is_dry_run:
            os.popen('cp "%s" %s' % (file, dst_dir + src_dir + split_filename[1]))
        return (split_filename[0], split_filename[1])

def add_image_in_csv(url_prefix, img_ugs, new_name, rows):
    ugs_list = [elt[1] for elt in rows]
    is_found = False
    for idx, ugs in enumerate(ugs_list):
        sub_parfum_ugs = -1
        is_ugs_ok = False
        if ugs == img_ugs:
            is_ugs_ok = True
        elif (ugs.find(img_ugs) == 0):
            try:
                sub_parfum_ugs = int(ugs[len(img_ugs):], base=10)
            except:
                print("Error: bad sub parfum ugs:{ugs}, img_ugs:{img_ugs} sub_parfum_ugs:{sub_ugs}"
                       .format(img_ugs=img_ugs, ugs=ugs, sub_ugs=ugs[len(img_ugs):]))
            is_ugs_ok = (sub_parfum_ugs in range(0,117))
        if is_ugs_ok:
            extra_coma = "" if len(rows[idx][5]) == 0 else ", "
            rows[idx][5] += extra_coma + url_prefix + urllib.parse.quote(new_name)
            is_found = True

    if not is_found:
        print("Error: ugs {img_ugs} not found in CSV for image {name}".format(img_ugs=img_ugs,
                                                                          name=new_name))
    return rows

def get_srv_subpath(new_name, srv_list):
    for srv_img in srv_list:
        if new_name in srv_img:
            #ret = ... + srv_img.replace(new_name,"").split("uploads")[0]
            ret = "wp-content/cache/tmp/" + srv_img.replace(new_name,"").split("uploads")[0]
            return ret
    return "wp-content/cache/tmp/"

################################################################################
# A) ID = 0
# B) UGS = 1
# C) Name = 2
# D) Parent = 3
# E) Type = 4
# F) Image = 5
# G) DESCRIPTION = 6
# H) DESCRIPTION COURTE = 7
# I) Published = 8
# J) Position = 9
# K) Regular Price = 10
# L) Sale Price = 11
# M) Attribute 1 name =  12
# N) Attribute 1 value(s) = 13
# O) Attribute 1 visible = 14
# P) Attribute 1 global = 15
# Q) Attribute 2 name = 16
# R) Attribute 2 value(s) = 17
# S) Attribute 2 visible = 18
# T) Attribute 2 global = 19
# U) Attribute 3 name = 20
# V) Attribute 3 value(s) = 21
# X) DISABLED Attribute 3 visible = 22
# Y) DISABLED Attribute 3 global = 23
# Z) Tax status = 24
# AZ) Tax class = 25
# AB) In stock? = 26
# AC) STOCK = 27
# AD) Backorders allowed? = 28
# AE) Longueur (cm) = 29
# AF) Hauteur (cm) = 30
# AG) Largeur (cm) = 31
# AH) Poids (kg) = 32
# AI) Allow customer reviews? = 33
# AJ) CATEGORIES = 34
# AK) Tags = 35
# AL) Meta: _wpcom_is_markdown = 36


def main():
    # 0) Parse argument
    mode = sys.argv[1]
    in_filename = sys.argv[2]
    src_dir = sys.argv[3]
    srv_dir = sys.argv[4] if len(sys.argv) > 4 else ""
    dst_dir = sys.argv[5] if len(sys.argv) > 5 else "out"

    if src_dir[-1] != '/':
        src_dir += "/"
    if dst_dir[-1] != '/':
        dst_dir += "/"
    if mode == "csv" and srv_dir == "":
        print("Error: for csv mode, srv_dir (arg4) must be given in parameter")
        return

    # 1) Local variable declaration
    out_filename = dst_dir + os.path.basename(in_filename)

    # 2) Core processing

    # Open the files
    in_file = open(in_filename)
    extra_ugs_file = open("extra_UGS_list.csv")
    if mode == "csv":
        out_file = try_open(out_filename)
    # Convert it as csv object
    csvreader = csv.reader(in_file)
    extra_csvreader = csv.reader(extra_ugs_file)
    # Convert it as csv writter
    if mode == "csv":
        csvwriter = csv.writer(out_file)
    # Get first row (header)
    header = next(csvreader)
    extra_header = next(extra_csvreader)
    if mode == "csv":
        csvwriter.writerow(header)
    # Get others row (data)
    rows = [row for row in csvreader]
    extra_rows =  [row for row in extra_csvreader]
    # Close reader
    in_file.close()
    extra_ugs_file.close()

    # Find the image list
    img_list = glob.glob("**", root_dir=src_dir, recursive=True)
    img_list = [src_dir + f for f in img_list if os.path.isfile(src_dir + f)]
    srv_list = glob.glob("**", root_dir=srv_dir, recursive=True)
    # Load extra ugs list
    primary_ugs_dict = {}
    primary_ugs_array = [row[0] for row in extra_rows]
    for idx, primary_ugs in enumerate(primary_ugs_array):
        primary_ugs_dict[primary_ugs] = extra_rows[idx][1:]

    # Clear the Image list
    for line in rows:
        line[5] = ""
    # Iterate over the images
    for img in img_list:
        # Rename the image (with copy)
        ugs, new_name = copy_and_rename(img, src_dir, dst_dir, is_dry_run=(mode == "csv"))
        srv_subpath = get_srv_subpath(new_name, srv_list)
        url_prefix = "http://www.prod-alba.com:8080/" + srv_subpath
        url_prefix = "https://www.prod-alba.com/" + srv_subpath
        # If rename/ugs parsing failed do not add it in csv
        if ugs != -1:
            # Add into the CSV
            rows = add_image_in_csv(url_prefix, ugs, new_name, rows)
        if ugs in primary_ugs_dict.keys():
            for extra_ugs in primary_ugs_dict[ugs]:
                rows = add_image_in_csv(url_prefix, extra_ugs, new_name, rows)

    if mode == "csv":
        csvwriter.writerows(rows)
        # Close the file
        out_file.close()

    print(("{nb_img} images have been proccess (%s)" %
            ("images renaming with no UGS prefix" if mode != "csv" else "images url loading in CSV ({csv_name})"))
          .format(nb_img=len(img_list), csv_name=out_filename))

main()
