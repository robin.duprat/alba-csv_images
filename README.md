# Alba CSV image adder


## Prerequisite

Python3 installed.
> -> See here [https://www.python.org/downloads/](https://www.python.org/downloads/).

## Run the csw_images.py

```
$ ./csw_images.py <my_input_csv_filename> <my_img_folder> <based_url> <optional_output_dir>
```

where :
-  ```<my_csv_filename>```  is your own CSV file name
-  ```<my_img_folder>``` is the directory of images to load (should be in project root directory)
-  ```<based_url>``` is the web site address, used as prefix for CSV url generation
-  ```<optional_output_dir>``` is an optionnal argument to indicate the name of the output directory (out/ by default)


## Limitations/ Warning
1) You need to have your <my_img_folder> in the same directory than the script, and use relative path for <my_img_folder> argument. This permit to directly get the path to append to the <based_url> for URL generation.

## Run the examples test (bash needed !)

```
$ ./run_test.sh
`
